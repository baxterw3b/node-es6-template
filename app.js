import express from 'express'
import morgan from 'morgan'
import bp from 'body-parser'

const PORT = process.env.PORT || 3000

const app = express()

app.use(morgan('combined'))
app.use(bp.json())

app.get('/', (req, res) => {
    res.send('hello node!')
})

app.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`)
})
