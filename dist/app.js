'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PORT = process.env.PORT || 3000;

var app = (0, _express2.default)();

app.use((0, _morgan2.default)('combined'));
app.use(_bodyParser2.default.json());

app.get('/', function (req, res) {
    res.send('hello node!');
});

app.listen(PORT, function () {
    console.log('Server started on http://localhost:' + PORT);
});